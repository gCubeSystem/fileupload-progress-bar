
# Changelog for File Upload Progress bar widget

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.0.0-SNAPSHOT] - 2022-06-15

- Ported to git
- Ported to GWT 2.9

## [v1.4.1] - 2016-01-27

- added support to drag and drop mechanism in share-updates

## [v1.2.0] - 014-07-17"

- Fixed upload File fails if file name contains chars like (à ù ò)

## [v1.1.0] - 2014-04-22

- Fixed gets stuck after 4/5 uploads in a row bug
- Fixed gets stuck if file size greater than 2GB

## [v1.0.0] - 2014-01-30

- First release
